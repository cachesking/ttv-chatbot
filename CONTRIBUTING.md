# How to Contribute to Kelp

Please read the [Contribution Guide below](#contribution-guide)

## Contribution Guide

### Getting Started

* Make sure you have a [Gitlab account](https://gitlab.com).
* Fork the repository and start working on your change.
* When your change is ready then [submit a Pull Request].

#### Minor Changes

For low-impact changes (ex: comments, documentation), it is not always necessary to create a new GitHub issue. In this case, it is appropriate to start the first line of a commit with 'doc' instead of an issue number.

### Finding things to work on

The first place to start is always looking over the [open GitHub issues]() for the project you are interested in contributing to. Issues marked with [help wanted]() are usually pretty self-contained and a good place to get started.


### Making Changes

* Create a feature branch _in your fork_ from where you want to base your work.
  * It is most common to base branch on the `master` branch.
  * Please avoid working directly on the `master` branch.
* Follow the code conventions of the existing repo. If you are adding a new file, please follow the Directory Structure in the [README](README.md#directory-structure).
* Make sure you have added the necessary tests for your changes and make sure all tests pass.
* Run your code against the test network to ensure that everything works.
* Update the README and walkthroughs if needed.

### Submitting Changes

* All content, comments, and pull requests must follow the [Community Guidelines]().
* Push your changes to a feature branch in your fork of the repository.
* Submit a Pull Request.
  * Include a descriptive [commit message](https://github.com/erlang/otp/wiki/Writing-good-commit-messages).
  * Include a link to your Github issue. Changes contributed via a pull request should focus on a single issue at a time.
  * Rebase your local changes against the `master` branch. Resolve any conflicts that arise.

At this point you're waiting on us. We like to at least comment on pull requests within three business days (typically, one business day). We may suggest some changes, improvements or alternatives.

## Additional Resources


This document is inspired by:

* https://github.com/stellar/docs/blob/master/CONTRIBUTING.md

package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"os"
)

const short = ""
const long = `
~/     https://twitch.tv/cachesking
~/code https://gitlab.com/cachesking`

const example = "\n  @cachesking projects --help"

var showtime string

// RootCmd is the main command for this repo
var RootCmd = &cobra.Command{
	Use:     "@cachesking",
	Short:   short,
	Long:    long,
	Example: example,
	Run: func(ccmd *cobra.Command, args []string) {
		intro := `
 ██████  █████   ██████ ██   ██ ███████ ███████ ██   ██ ██ ███    ██  ██████ 
██      ██   ██ ██      ██   ██ ██      ██      ██  ██  ██ ████   ██ ██      
██      ███████ ██      ███████ █████   ███████ █████   ██ ██ ██  ██ ██   ███ 
██      ██   ██ ██      ██   ██ ██           ██ ██  ██  ██ ██  ██ ██ ██    ██ 
 ██████ ██   ██  ██████ ██   ██ ███████ ███████ ██   ██ ██ ██   ████  ██████  
		`
		fmt.Println(intro)
		e := ccmd.Help()
		if e != nil {
			panic(e)
		}
		ccmd.SetOut(os.Stdout)
	},
}

func init() {
	RootCmd.AddCommand(aboutCmd)
	RootCmd.AddCommand(beginbotCmd)
	RootCmd.AddCommand(projectsCmd)
	RootCmd.AddCommand(rigCmd)
	RootCmd.AddCommand(scheduleCmd)
	RootCmd.AddCommand(cacheCmd)
}

package cmd

import (
	"github.com/gempir/go-twitch-irc"
	"github.com/spf13/cobra"
	"gitlab.com/cachesking/go/support/scribe"
	"gitlab.com/cachesking/ttv-chatbot/strategy"
	"gitlab.com/cachesking/ttv-chatbot/support/utils"
	"os"
)

// New up cobra command to gracefully handle command line flags.
var cacheCmd = &cobra.Command{
	Use:   "init",
	Short: "irc world builder",
}

// New up the  modus operandi (mo) and strategy broker.
var (
	broker = strategy.NewBroker()
	// greet by username
	hello = strategy.Hello{Name: "hello"}
	// dab on the nerds
	dab = strategy.Dabricorn{Name: "dab"}
	// troll the nerds
	troll = strategy.Troll{Name: "troll"}

	// newup a scribe to scrible notes
	scrbl = scribe.Configure(scribe.Config{
		ConsoleLogs: true,
		FileLogs:    true,
		JsonEncode:  true,
		Directory:   logspath,
		Filename:    "cache.logs",
		MaxSize:     20,
		MaxBackups:  10,
		MaxAge:      3,
	})
)

func init() {

	// Add bot's mo one by one
	// TODO: strategy.NewBrokerFromMo(mo ModusOperandi) *Broker
	broker.Add(hello.Name, hello.Execute)
	broker.Add(dab.Name, dab.Execute)
	broker.Add(troll.Name, troll.Execute)

	// CobraCommand entry point.Start the twitc listener and lay
	// out our strategy as it applies the `*twitch.Client` events.
	cacheCmd.Run = func(ccmd *cobra.Command, args []string) {
		// dat me
		h := "cachesking"
		// destination. i.e. the channel you want to /JOIN
		channel := h

		scrbl.Info().
			Str("host", h).
			Str("channel", channel).
			Msg("Connecting...")

		// Ask the twitch factory for a shiny new irc client.
		// Get your ticket to ride: `https://twitchapps.com/tmi/`

		// Credentials should be handled with care. `git` helps
		// by providing conventions to follow in order to keep
		// secrets safe. This project should contain `.gitignore`
		// and `.env` files. If not: stop now and research what
		// these are and why they are important.
		//
		// NOTE: dot env: keeps environment specific information
		//       in a key value pair. e.g. KEY=value
		//
		//       gitignore: ignores files listed within it
		//
		// TODO: check that environment secrets stored securely
		//       and the appropriate files are ignored
		client := twitch.NewClient(h, os.Getenv("ACCESS_TOKEN"))

		// Please stand up and tell the class about event and
		// what they do. Cient, would you start?
		client.OnConnect(func() {
			// Scrible some basics here and there. This will
			// be command and should be self documenting.
			scrbl.Info().
				Str("channel", channel).
				Msg("Connected.")
		})

		client.OnPrivateMessage(func(m twitch.PrivateMessage) {
			// DRAFT: Private messages are the public the
			// to the channel and therefore will house the
			// bulk of the bot's strategy handling.

			// Know who you're dealing with:
			// Determine if the message has any commands.
			// Providing different `leader` characters can
			// surface alternative command types.
			// e.g. double bang (!!) for custom or question
			// marks for help commands
			cmd, args := utils.ParseMessage("!", m.Message)
			// TODO: Parse @users out of the returned args

			if cmd != "" {
				scrbl.Debug().
					Str("cmd", cmd).
					Strs("args", args).
					Str("user", m.User.Name).
					Msg("")

				// MetaInfo contextual data to pass to the
				// execution of a strategy. This type will
				// grow alongside strategy complexity and
				// will require new Meta Types and Execution
				// method patterns in support of this.
				meta := strategy.MetaInfo{
					Channel: channel,
					From:    m.User.Name,
				}

				// Do the damn thang. The strategy pattern
				// and broker are for enacting complex
				// tactics for interacting within internet
				// relay chat. Not all interactions will
				// require the need for this pattern.
				//
				// KISC: Keep it simple, cadet.
				broker.Execute(cmd, client, args, meta)
				// Poor space cowboys method missing. Ruby
				// has this sweet pattern where methods that
				// haven't resolved to a know method, can
				// be handled in a `finally` manner.
				// this should be in the broker.
			} else {
				// if nothing else, scribble it down.
				scrbl.Info().
					Str("user", m.User.Name).
					Msg(m.Message)
			}
		})

		client.OnUserJoinMessage(func(m twitch.UserJoinMessage) {
			// Welcome to `WhateverWorld`, space cadet.
			scrbl.Info().Str("user", m.User).Msg("joined")
			// Determine citizenship status

			// Citizens have an index page in the passport list.
			// Visitors are on the planet probationarily and
			// must interact to maintain citizenship.

			// Lowest bar upload an avatar, cadet.
		})

		client.Join(channel)

		err := client.Connect()
		if err != nil {
			scrbl.Fatal().
				Err(err).
				Str("channel", channel).
				Msg("")
		}
	}
}

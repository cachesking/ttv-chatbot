package cmd

import (
	"log"

	"github.com/spf13/cobra"
)

var scheduleCmd = &cobra.Command{
	Use:   "schedule",
	Short: "Tuesdays @ 00:00 UTC",
	Run: func(ccmd *cobra.Command, args []string) {
		log.Printf("Tuesdays @ 00:00 UTC")
	},
}

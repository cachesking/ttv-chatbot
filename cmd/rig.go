package cmd

import (
	"log"

	"github.com/spf13/cobra"
)

var rigCmd = &cobra.Command{
	Use:   "rig",
	Short: "zsh vim and tmux on mbp",
	Run: func(ccmd *cobra.Command, args []string) {
		log.Printf("Dracula themed vim and tmux on mbp")
	},
}

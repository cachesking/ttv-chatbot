package cmd

import (
	"log"

	"github.com/spf13/cobra"
)

var funCmd = &cobra.Command{
	Use:   "fun",
	Short: "always true",
	Run: func(ccmd *cobra.Command, args []string) {
		log.Printf("always true")
	},
}

package cmd

import (
	"log"

	"github.com/spf13/cobra"
)

var botCmd = &cobra.Command{
	Use:   "commands",
	Short: "!help !me",
	Run: func(ccmd *cobra.Command, args []string) {
		log.Printf("!help !me")
	},
}

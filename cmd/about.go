package cmd

import (
	"log"

	"github.com/spf13/cobra"
)

var aboutCmd = &cobra.Command{
	Use:   "about",
	Short: "Everything's for my L1 and L2 cache. Love you <3",
	Run: func(ccmd *cobra.Command, args []string) {
		log.Printf("Everything's for my L1 and L2 cache. Love you <3")
	},
}

package cmd

import (
	"fmt"
	"github.com/gempir/go-twitch-irc"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"gitlab.com/cachesking/ttv-chatbot/support/utils"
	"os"
)

var (
	homepath = os.Getenv("HOME")
	gopath   = os.Getenv("GOPATH")
	projpath = "src/gitlab.com/cachesking/ttv-chatbot"
	workpath = fmt.Sprintf("%s/%s", gopath, projpath)
	logspath = fmt.Sprintf("%s/logs", workpath)
)

var beginbotCmd = &cobra.Command{
	Use:   "beginbot",
	Short: "CacheMachine go brrrr",
}

func init() {
	beginbotCmd.Run = func(ccmd *cobra.Command, args []string) {
		h := os.Getenv("BOT")
		channel := "beginbot"

		log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
		log.Info().Str("channel", channel).Msg("connecting")
		client := twitch.NewClient(h, os.Getenv("BOTKEY"))

		client.OnConnect(func() {
			log.Info().Msg("connected")
		})

		client.OnWhisperMessage(func(m twitch.WhisperMessage) {
			log.Debug().Str("user", m.User.Name).Msg(m.Message)
			if m.User.Name == "cachesking" {
				cmd, args := utils.ParseMessage("!!", m.Message)
				if cmd == "thank" {
					client.Say(channel, fmt.Sprintf("Cache sends you warm regards, %s", args[0]))
				}
			}
		})

		client.OnPrivateMessage(func(m twitch.PrivateMessage) {
			log.Debug().
				Str("user", m.User.Name).
				Msg(m.Message)

			cmd, args := utils.ParseMessage("!", m.Message)
			if cmd != "" {
				log.Info().
					Str("user", m.User.Name).
					Str("cmd", cmd).
					Strs("args", args).
					Msg("")
			}
		})

		client.OnUserJoinMessage(func(m twitch.UserJoinMessage) {
			log.Info().
				Str("user", m.User).
				Msg("joined")
		})

		client.Join(channel)

		err := client.Connect()
		if err != nil {
			log.Fatal().
				Err(err).
				Str("channel", h).
				Msg("failed to connect")
		}
	}
}

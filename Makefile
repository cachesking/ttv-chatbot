LOGPATH = $(GOPATH)/src/gitlab.com/cachesking/ttv-chatbot/logs
LOGDATE = $(shell date --iso=seconds)
CACHE   = $(LOGPATH)/cache-$(LOGDATE).log
BEING   = $(LOGPATH)/beginbot-$(LOGDATE).log

beginbot:
	go run main.go beginbot
cache:
	go run main.go init
run:
	go run main.go | lolcat

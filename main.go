package main

import (
	"fmt"
	"github.com/joho/godotenv"
	"gitlab.com/cachesking/go/support/scribe"
	"gitlab.com/cachesking/ttv-chatbot/cmd"
	"os"
)

var (
	homepath = os.Getenv("HOME")
	gopath   = os.Getenv("GOPATH")
	projpath = "src/gitlab.com/cachesking/ttv-chatbot"
	workpath = fmt.Sprintf("%s/%s", gopath, projpath)
	logspath = fmt.Sprintf("%s/logs", workpath)
)

func init() {
	scrbl := scribe.Configure(scribe.Config{ConsoleLogs: true})
	err := godotenv.Load()
	if err != nil {
		scrbl.Fatal().Err(err).Msg("Come on, cache!")
	}
}

func main() {
	cmd.RootCmd.Execute()
}

package strategy

import (
	"github.com/gempir/go-twitch-irc"
	"gitlab.com/cachesking/ttv-chatbot/support/leap"
)

type Troll struct {
	Name string
}

func (t *Troll) Execute(c *twitch.Client, args []string, m MetaInfo) {
	params := leap.Params{
		Type:    "trolls",
		ImgId:   "1",
		Sparkle: 1,
		Number:  "0",
	}
	if len(args) > 0 {
		params.Number = args[0]
	}
	leap.Toss(params)
}

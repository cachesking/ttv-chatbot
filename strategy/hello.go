package strategy

import (
	"fmt"
	"github.com/gempir/go-twitch-irc"
)

type Hello struct {
	Name string
}

func (h Hello) Execute(c *twitch.Client, args []string, m MetaInfo) {
	c.Say(m.Channel, fmt.Sprintf("hello @%s", m.From))
}

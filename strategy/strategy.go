package strategy

import (
	"fmt"
	"github.com/gempir/go-twitch-irc"
)

// MetaInfo strategy configuration sent by the caller
type MetaInfo struct {
	// Channel to send to
	Channel string
	// Message sender
	From string
}

// Executable strategies are ones that act on the client given
// any supporting argumens and message meta data
type Executable func(*twitch.Client, []string, MetaInfo)

// StrategyCache is a map of named executables
type StrategyCache map[string]Executable

// Broker load and take action one or more strategies
type Broker struct {
	Strategies StrategyCache
}

// NewBroker broker factory. New up a strategy broker
func NewBroker() *Broker {
	return &Broker{Strategies: make(StrategyCache)}
}

// Add a single strategy {k, v} pair to broker's cache
func (b Broker) Add(name string, e Executable) {
	b.Strategies[name] = e
}

// Execute strategy by name providing optional argumens and meta data
func (b Broker) Execute(name string, c *twitch.Client, args []string, m MetaInfo) {
	execute, ok := b.Strategies[name]
	if ok {
		execute(c, args, m)
	} else {
		reply := fmt.Sprintf("requestor:  @%s %s", m.From, name)
		c.Say(m.Channel, reply)
	}
}

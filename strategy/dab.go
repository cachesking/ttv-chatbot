package strategy

import (
	"github.com/gempir/go-twitch-irc"
	"gitlab.com/cachesking/ttv-chatbot/support/leap"
)

type Dabricorn struct {
	Name string
}

func (d Dabricorn) Execute(c *twitch.Client, args []string, m MetaInfo) {
	params := leap.Params{Number: "0"}
	if len(args) > 0 {
		params.Number = args[0]
	}
	leap.Toss(params)
}

package utils

import (
	"fmt"
	"gitlab.com/cachesking/go/support/scribe"
	"math/rand"
	"os"
	"strconv"
	"strings"
	"time"
)

var (
	scrbl = scribe.Configure(scribe.Config{ConsoleLogs: true})
)

func init() {
	rand.Seed(time.Now().Unix())
}

// ParseMessage Checks the first byte and returns a command and it's
// arguments. commands are designated by their leader
func ParseMessage(leader string, m string) (cmd string, args []string) {
	b := []byte(leader)[0]
	first := m[0]
	if first == b {
		parts := strings.Split(m, " ")
		cmd := strings.TrimPrefix(parts[0], leader)
		if len(parts) == 1 {
			return cmd, make([]string, 0)
		}
		return cmd, parts[1:]
	}
	return "", make([]string, 0)
}

func Atoi(s string) int {
	i, err := strconv.Atoi(s)
	if err != nil {
		scrbl.Warn().
			Err(err).
			Msgf("%s", s)
		return 0
	}
	return i
}

func Itoa(i int) string {
	return strconv.Itoa(i)
}

func Getdir(name string) string {
	wd, err := os.Getwd()
	if err != nil {
		scrbl.Error().
			Err(err).
			Msg("get wd")
	}

	if len(name) < 1 {
		return wd
	} else {
		return fmt.Sprintf("%s/%s", wd, name)
	}
}

// Get a list of assets in a given type
func Inventory(dir string) []string {
	// read in the directory
	file, err := os.Open(dir)
	if err != nil {
		scrbl.Error().
			Str("dir", dir).
			Msg("")
	}
	defer file.Close()

	names, _ := file.Readdirnames(0)
	return names
}

func Random(min int, max int) int {
	rand.Seed(time.Now().Unix())
	return rand.Intn(max-min) + min
}

// RandomSample selects a random item from the given list
func RandomSample(list []string) string {
	rand.Seed(time.Now().Unix())
	return list[rand.Intn(len(list))]
}

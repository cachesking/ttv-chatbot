package leap

import (
	"fmt"
	"gitlab.com/cachesking/go/support/scribe"
	"gitlab.com/cachesking/ttv-chatbot/support/colonel"
	"gitlab.com/cachesking/ttv-chatbot/support/utils"
	"os"
	"strings"
)

var (
	basepath    = utils.Getdir("assets")
	unicornpath = ""
)

type Params struct {
	// T Type directory name e.g. robot or troll
	Type string
	// ID Assets are saved as ID.png
	ImgId string
	// Number of images to send
	Number      string
	Ecentricity string
	// zero sparkle one clunk
	Sparkle int
	// Hoard of images
	Herd bool
}

// Usage: unicornleap [options]
//    -h  --help             Display usage information.
//    -s  --seconds n        Animate for n seconds. (default: 2.0)
//    -n  --number i         Display i unicorns. (default: 1)
//    -H  --herd             Enables herd-mode.
//    -e  --eccentricity x   Height. (default: 1.0)
//    -u  --unicorn file     Filename for unicorn image.
//    -k  --sparkle file     Filename for sparkle image.
func Toss(toss Params) {
	scrbl := scribe.Configure(scribe.Config{ConsoleLogs: true})
	// new up our builder
	cmdr := colonel.NewCommander("unicornleap", []string{"-u"})

	assetpath := fmt.Sprintf("%s/%s", basepath, toss.Type)
	assetpath = strings.TrimRight(assetpath, "/")
	// evaluate the paramaters
	// default to dabricorn if provided zero params
	// select a random image if only a type is given
	// validate that provided img exists
	// if not default to random image
	if toss.Type == "" && toss.ImgId == "" {
		cmdr.Add(basepath + "/dabricorn.png")
	} else if toss.Type != "" && toss.ImgId == "" {
		// list all the assets in the type
		names := utils.Inventory(assetpath)
		// randomly sample one of the assets
		img := utils.RandomSample(names)
		// build the asset path
		unicornpath = fmt.Sprintf("%s/%s", assetpath, img)
	}

	// configure image name. must be a dot png
	if len(toss.Type) > 0 && len(toss.ImgId) > 0 {
		// path of image msgid.png
		imgpath := fmt.Sprintf("%s.png", toss.ImgId)
		// combine for the unicornpath
		unicornpath = fmt.Sprintf("%s/%s", assetpath, imgpath)

		// fall back to something that exists
		// in case we built a trash link
		if _, err := os.Stat(unicornpath); err == nil {
			cmdr.Add(unicornpath)
		} else if os.IsNotExist(err) {
			scrbl.Error().
				Err(err).
				Str("path", unicornpath).
				Msg("")

			toss.Sparkle = 1
			cmdr.Add(basepath + "/dabricorn.png")
		}
	}

	// Set sparkles by exit code. 0 sparke 1 clunk
	cmdr.Add("-k")
	if toss.Sparkle > 0 {
		cmdr.Add(utils.Getdir("assets") + "/clunk.png")
	} else {
		cmdr.Add(utils.Getdir("assets") + "/sparkle.png")
	}

	// Number of images to send
	// set sane guard rails to prevent spam
	number := toss.Number
	count := utils.Atoi(number)

	// lower bound
	if count < 1 {
		number = "1"
	}

	// upper bound
	if count > 3 {
		number = "3"
	}

	cmdr.Add("-n")
	cmdr.Add(number)

	// set a random excentricity
	cmdr.Add("-e")
	r := utils.Random(1, 4)
	e := utils.Itoa(r)
	cmdr.Add(fmt.Sprintf("%s.0", e))

	// run commander is non blocking
	cmdr.RunCmd()

}

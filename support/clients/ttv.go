package ttv

import (
	"github.com/gempir/go-twitch-irc"
	"github.com/joho/godotenv"
	"log"
	"os"
)

func NewClient() *twitch.Client {
	// Load .env into os
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Do you even secret?")
	}

	t := os.Getenv("ACCESS_TOKEN")
	h := os.Getenv("HANDLE")

	return twitch.NewClient(h, t)
}

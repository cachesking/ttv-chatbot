package colonel

import (
	"gitlab.com/cachesking/go/support/scribe"
	"os/exec"
)

var (
	scrbl = scribe.Configure(scribe.Config{ConsoleLogs: true})
)

type CommandBuilder interface {
	Add(string)
	RunCmd()
}

type Commander struct {
	cmd string
	arg []string
}

func (c *Commander) Add(arg string) {
	c.arg = append(c.arg, arg)
}

func (c *Commander) RunCmd() {
	scrbl.Debug().
		Str("command", c.cmd).
		Msg("Runing")

	if err := exec.Command(c.cmd, c.arg...).Run(); err != nil {
		scrbl.Warn().
			Err(err).
			Msg("")
	}
}

func NewCommander(name string, arg []string) *Commander {
	c := Commander{cmd: name, arg: arg}
	return &c
}
